package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.Model.Region;

@Service
public class RegionService {
     
        Region dongnai = new Region("60", "Đồng Nai");
        Region saigon = new Region("59", "Sài Gòn");
        Region hanoi = new Region("29", "Hà Nội");
        Region texas = new Region("43", "Texax");
        Region florida = new Region("35", "Florida");
        Region newyork = new Region("78", "New York");
        Region okinawa = new Region("49", "Okinawa");
        Region tokyo = new Region("48", "Tokyo");
        Region kyoto = new Region("81", "Kyoto");

        public ArrayList<Region> getRegionVN(){
            ArrayList<Region> regionsVN = new ArrayList<Region>();
    
           regionsVN.add(dongnai);
           regionsVN.add(saigon);
           regionsVN.add(hanoi);

            return regionsVN;
        }
        public ArrayList<Region> getRegionJP(){
            ArrayList<Region> regionsJP = new ArrayList<Region>();
    
            regionsJP.add(okinawa);
            regionsJP.add(tokyo);
            regionsJP.add(kyoto);
    
            return regionsJP;
        }
        public ArrayList<Region> getRegionUS(){
            ArrayList<Region> regionsUS = new ArrayList<Region>();
    
            regionsUS.add(texas);
            regionsUS.add(florida);
            regionsUS.add(newyork);
    
            return regionsUS;
        }
    
        public ArrayList<Region> getRegionsAll() {
            ArrayList<Region> regionsAll = new ArrayList<Region>();

            regionsAll.add(dongnai);
            regionsAll.add(saigon);
            regionsAll.add(hanoi);
            regionsAll.add(okinawa);
            regionsAll.add(tokyo);
            regionsAll.add(kyoto);
            regionsAll.add(texas);
            regionsAll.add(florida);
            regionsAll.add(newyork);
    
            return regionsAll;
            
        }
    }