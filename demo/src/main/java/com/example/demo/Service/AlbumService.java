package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Album;

@Service
public class AlbumService {
    Album NooPhuocThinh = new Album(1, "vol1" , new ArrayList<String>(){
        {
            add(new String("Chờ ngày mưa tan"));
            add(new String("Thương em là điều anh không ngờ"));
        }
    });
    Album VuCatTuong = new Album(2, "vol2" , new ArrayList<String>(){
        {
            add(new String("Yêu xa"));
            add(new String("Có người"));
        }
    });
    Album BaoAnh = new Album(2, "vol3" , new ArrayList<String>(){
        {
            add(new String("Yêu một người vô tâm"));
            add(new String("Như lời đồn"));
        }
    });
    

    public ArrayList<Album> getAlbumNooPhuocThinh(){
        ArrayList<Album> NooPhuocThinhVol1 = new ArrayList<Album>();

        NooPhuocThinhVol1.add(NooPhuocThinh);

        return NooPhuocThinhVol1;
    }

    public ArrayList<Album> getAlbumVuCatTuong(){
        ArrayList<Album> VuCatTuongVol2 = new ArrayList<Album>();

        VuCatTuongVol2.add(VuCatTuong);

        return VuCatTuongVol2;
    }
    public ArrayList<Album> getAlbumBaoAnh(){
        ArrayList<Album> BaoAnhVol3 = new ArrayList<Album>();

        BaoAnhVol3.add(BaoAnh);

        return BaoAnhVol3;
    }
    

    public ArrayList<Album> getAllAlbum(){
        ArrayList<Album> AllAlbum = new ArrayList<Album>();

        AllAlbum.add(NooPhuocThinh);
        AllAlbum.add(VuCatTuong);
        AllAlbum.add(BaoAnh);

        return AllAlbum;
    }
}
